#include <stdio.h>
#include <windows.h>

#include <iostream>

#pragma warning(disable : 4996)

using namespace std;

char token[80];
FILE *file;

int P(), D1(), D_DOP(), I_DOP(), I(), NAME(), TIP();

// ��������� ������ �� �����.
void GetCurrentLexeme()
{
	fscanf(file, "%s", token);
	return;
}

// ��������� ������� �������� ���� � �������� program, var � ;
int P()
{
	if(strcmp(token, "program") != 0)
		return 0;
	GetCurrentLexeme();
	if(strcmp(token, "var") != 0)
		return 0;
	GetCurrentLexeme();
	if(D1() == 0)
		return 0;
	if(strcmp(token, ";") != 0)
		return 0;
	return 1;
}

// ��������� ������� :
int D1()
{
	if(I() == 0)
		return 0;
	if(strcmp(token, ":") != 0)
		return 0;
	GetCurrentLexeme();
	if(TIP() == 0)
		return 0;
	GetCurrentLexeme();
	if(D_DOP() == 0)
		return 0;
	return 1;
}

// ��������� ������� , � ;
int D_DOP()
{
	if(strcmp(token, ",") == 0)
	{
		GetCurrentLexeme();
		D1();
	}
	else 
	{
		if(strcmp(token, ";") == 0)
			return 1;
		else
			return 0;
	}

}

// ��������� ������������ ���������� ����������.
int I()
{
	if(NAME() == 0)
		return 0;
	GetCurrentLexeme();
	if(I_DOP() == 0)
		return 0;
	return 1;
}

// ��������� ������� , � .
int I_DOP()
{
	if(strcmp(token, ",") == 0)
	{
		GetCurrentLexeme();
		I();
	}
	else 
	{
		if(strcmp(token, ":") == 0)
			return 1;
		else
			return 0;
	}

}

// ��������� ������������ �����
int TIP()
{
	if (strcmp(token, "int") == 0 || strcmp(token, "bool") == 0)
	{
		return 1;
	}
	
	return 0;
}

// ��������� ������������ ����
int NAME()
{
	for (int i = 0; token[i] != '\0'; i++)
	{
		char symbol = token[i];

		tolower(symbol);

		if ((symbol < 'a' || symbol > 'z') && (symbol < '0' || symbol > '9'))
		{
			return 0;
		}
	}
	
	return 1;
}

// ������� ����� ���������.
void main()
{
	int isCorrect;
	file = fopen("text.txt", "r");
	GetCurrentLexeme();
	isCorrect = P();
	fclose(file);

	if(isCorrect == 0)
		printf("Error\n");
	else
		printf("Correct expressions\n");

	while(1)
		if(getchar()==27)
			break;
}